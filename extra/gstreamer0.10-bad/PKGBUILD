# $Id: PKGBUILD 202975 2013-12-29 17:17:33Z bpiotrowski $
# Maintainer: Anthony Donnelly <amzo@archbsd.net>

pkgbase=gstreamer0.10-bad
pkgname=('gstreamer0.10-bad' 'gstreamer0.10-bad-plugins')
pkgver=0.10.23
pkgrel=13
arch=('i686' 'x86_64')
license=('LGPL' 'GPL')
makedepends=('pkg-config' 'gstreamer0.10-base>=0.10.36' 'xvidcore' 'libdca' 'libdc1394' 'neon' 'faac' 'musicbrainz' 'faad2' 'libmms' 'libcdaudio' 'libmpcdec' 'mjpegtools' 'libdvdnav' 'jasper' 'liblrdf' 
'libofa' 'soundtouch' 'libvdpau' 'schroedinger' 'libass' 'libvpx' 'gsm' 'libgme' 'rtmpdump' 'libsndfile' 'librsvg' 'wildmidi' 'opus' 'git' 'spandsp' 'celt')
url="http://gstreamer.freedesktop.org/"
options=(!emptydirs)
patchdir="gst-plugins-bad/"
source=("git://anongit.freedesktop.org/gstreamer-sdk/gst-plugins-bad#commit=57569a4854a0f2d14ef19a8264a4ae9a7a1d1125"
        disable-assrender-test.patch
        disable-camerabin-test.patch)
sha256sums=('SKIP'
            'e66642affa6c0e69837d37615010e67e59ef3d672663303d46c1e2591e2ddfc6'
            '01e780ddf1f8161a6115dded9dc5bf4bdd4d09a9eee00fa423b1330e90e76c68')

prepare() {
  cd gst-plugins-bad
  gsed -e 's/AM_CONFIG_HEADER/AC_CONFIG_HEADERS/' -i configure.ac
  patch -Np1 -i ../disable-assrender-test.patch
  patch -Np1 -i ../disable-camerabin-test.patch
}

build() {
  cd gst-plugins-bad
  NOCONFIGURE=1 ./autogen.sh
  ./configure --prefix=/usr --sysconfdir=/etc --localstatedir=/var \
    --disable-static --enable-experimental --disable-gtk-doc \
    --with-package-name="GStreamer Bad Plugins (ArchBSD)" \
    --with-package-origin="http://www.archbsd.net/" \
    --disable-apexsink \
    --disable-modplug
  gmake
  gsed -e 's/gst sys ext/gst/' -i Makefile
}

check() {
  cd gst-plugins-bad
  #two test failures.
  gmake check || true
}

package_gstreamer0.10-bad() {
  pkgdesc="GStreamer Multimedia Framework Bad Plugin libraries (gst-plugins-bad)"
  depends=('gstreamer0.10-base>=0.10.34')

  cd gst-plugins-bad
  gmake DESTDIR="${pkgdir}" install
}

package_gstreamer0.10-bad-plugins() {
  pkgdesc="GStreamer Multimedia Framework Bad Plugins (gst-plugins-bad)"
  depends=("gstreamer0.10-bad=${pkgver}" 'xvidcore' 'libdca' 'libdc1394' 'neon' 'faac' 'musicbrainz' 'faad2' 'libmms' 'libcdaudio' 'libmpcdec' 'mjpegtools' 'libdvdnav' 'libmodplug' 'jasper' 'liblrdf' 'libofa' 'libvdpau' 'soundtouch' 'libass' 'schroedinger' 'libvpx' 'gsm' 'rtmpdump' 'libgme' 'libsndfile' 'librsvg' 'wildmidi' 'opus' 'celt' 'spandsp')
  groups=('gstreamer0.10-plugins')
  install=gstreamer0.10-bad-plugins.install

  cd gst-plugins-bad
  gmake -C gst-libs DESTDIR="${pkgdir}" install
  gmake -C ext DESTDIR="${pkgdir}" install
  gmake -C sys DESTDIR="${pkgdir}" install
  gmake -C gst-libs DESTDIR="${pkgdir}" uninstall
}
