#
# Maintainer: Anthony Donnelly <Amzo@archbsd.net>

pkgbase=python-setuptools
pkgname=('python-setuptools' 'python2-setuptools')
pkgver=17.1.1
pkgrel=1
pkgdesc="Easily download, build, install, upgrade, and uninstall Python packages"
arch=('any')
license=('PSF')
url="http://pypi.python.org/pypi/setuptools"
makedepends=('python' 'python2')
checkdepends=('python-pytest' 'python2-pytest')
source=("https://pypi.python.org/packages/source/s/setuptools/setuptools-${pkgver}.tar.gz")
sha512sums=('69b32baaac2954d7a9fb4df8738489c6abec090a69dc44781e60fae302443f28628730d0b72d9fa19a14045df0e512b6830be6bc603b97f73c16e7ba29b3b9c9')

prepare() {
  cp -a setuptools-${pkgver}{,-python2}

  cd "${srcdir}"/setuptools-${pkgver}
  sed -i '' -e "s|^#\!.*/usr/bin/env python|#!/usr/bin/env python3|" setuptools/command/easy_install.py

  cd "${srcdir}"/setuptools-${pkgver}-python2
  sed -i '' -e "s|^#\!.*/usr/bin/env python|#!/usr/bin/env python2|" setuptools/command/easy_install.py
}

build() {
   # Build python 3 module
   cd "${srcdir}"/setuptools-${pkgver}
   python3 setup.py build
 
   # Build python 2 module
   cd ../setuptools-${pkgver}-python2
   python2 setup.py build
}
 
package_python-setuptools() {
   depends=('python>=3.4')
   provides=('python-distribute')
   replaces=('python-distribute')
 
   cd "${srcdir}/setuptools-${pkgver}"
   python3 setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
}
 
package_python2-setuptools() {
   depends=('python2>=2.7')
   provides=('python2-distribute' 'setuptools')
   replaces=('python2-distribute' 'setuptools')
 
   cd "${srcdir}/setuptools-${pkgver}-python2"
   python2 setup.py install --prefix=/usr --root="${pkgdir}" --optimize=1 --skip-build
   rm "${pkgdir}/usr/bin/easy_install"
}
